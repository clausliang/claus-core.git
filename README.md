# claus-core

## 介绍
js公共方法的集合


## 使用示例
安装
```bash
yarn add claus-core
```
使用
```js
import { getToday } from 'claus-core'
console.log(getToday()) // 2021年06月24号 星期四
```
## 各方法的参数及返回值
### 1.formatTime(timestamp[, format])
将时间戳转为时间字符串,format可不传默认'yyyy-MM-dd HH:mm:ss'
```js
console.log(formatTime(new Date(),'yyyy-MM-dd')) // 2021-11-18
console.log(formatTime(new Date(),'yyyy-MM-dd HH:mm:ss')) // 2021-11-18 17:25:14
console.log(formatTime(new Date())) // 2021-11-18 17:25:14
console.log(formatTime(new Date(),'yyyy/MM/dd HH:mm')) // 2021/11/18 17:25
```
### 2.getToday()
获取今天的年月日星期 如 2021年06月24号 星期四
```js
console.log(getToday()) // 2021年06月24号 星期四
```
### 3.formatMoney(money)
传入金额数字转为千分形式
```js
console.log(formatMoney(1234)) // 1,234.00
```
### 4.formatPhoneNumber(phone)
手机号脱敏
```js
console.log(formatPhoneNumber('17612348888')) // 176****8888
```
### 5.obj2Param(obj)
接收一个对象，转为key1=value1&key2=value2的参数字符串拼接
```js
console.log(obj2Param({name:'zhangsan',age:18})) // name=zhangsan&age=18
```
### 6.validateIdCard(idCard)
校验身份证号是否正确
```js
validateIdCard('141122199901011234') // false
```
### 7.validatePassport(pass)
校验护照（港澳通行证）是否正确
```js
validatePassport('141122199901011234') // false
```
### 8.formatBankCard(bankCard)
银行卡号四位分割
```js
console.log(formatBankCard('6214830194484993')) // 6214 8301 9448 4993 
```
## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

