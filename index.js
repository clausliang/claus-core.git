/** 1格式化时间
 * @name 格式化时间
 * @param {number | string} time 时间戳
 * @param {string} format 时间格式， 默认'yyyy-MM-dd HH:mm:ss'
 * @returns {string}
 */
export function formatTime(time, format) {
    if (!time) {
        return time
    }
    let currentFormat = format || 'yyyy-MM-dd HH:mm:ss'
    let date = new Date(time)
    // 非时间格式返回自身
    if (isNaN(Number(date))) {
        return time
    }
    let o = {
        'M+': date.getMonth() + 1,
        'd+': date.getDate(),
        'H+': date.getHours(),
        'm+': date.getMinutes(),
        's+': date.getSeconds(),
        'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
        'f+': date.getMilliseconds() // 毫秒
    };
    if (/(y+)/.test(currentFormat)) {
        currentFormat = currentFormat.replace(RegExp.$1, (String(date.getFullYear())).substr(4 - RegExp.$1.length));
    }
    for (let k in o) {
        if (new RegExp('(' + k + ')').test(currentFormat)) {
            currentFormat = currentFormat.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr((String(o[k])).length)));
        }
    }
    return currentFormat;
}

/**
 * 2返回年月日 星期
 * @returns 2021年06月24号 星期四
 */
export function getToday() {
    const time = new Date()
    let dateStr = ''
    const year = time.getFullYear()
    const month = String(time.getMonth() + 1).length === 1 ? '0' + (time.getMonth() + 1) : (time.getMonth() + 1)
    const day = String(time.getDate()).length === 1 ? '0' + time.getDate() : time.getDate()
    const xingqi = ['日', '一', '二', '三', '四', '五', '六'][time.getDay()]
    dateStr = year + '年' + month + '月' + day + '日' + ' 星期' + xingqi
    return dateStr
}

/**
 * 3金额千分符
 */
export function formatMoney(money) {
    if (money) {
        let moneyArr = (+money).toFixed(2).split(".");
        return moneyArr[0].replace(/(\d)(?=(?:\d{3})+$)/g, '$1,') + "." + moneyArr[1]
    } else {
        return money
    }
}

/**
 * 4手机号脱敏
 * @param {*} phonne 
 * @returns 
 */
export function formatPhoneNumber(phone) {
    if (phone) {
        return phone.slice(0, 3) + '****' + phone.slice(7)
    }
}

/**
 * 5对象转参数字符串拼接
 * @param {*} object 
 */
export function obj2Param(object) {
    let params = [];
    for (const key in object) {
        if (object.hasOwnProperty(key)) {
            const element = object[key];
            params.push(key + '=' + object[key])
        }
    }
    return params.join('&')
}

/**
 * 6校验身份证号是否正确
 * @param idCard
 */
export function validateIdCard(idCard) {
    //15位和18位身份证号码的正则表达式
    var regIdCard = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;
    //如果通过该验证，说明身份证格式正确，但准确性还需计算
    if (regIdCard.test(idCard)) {
        if (idCard.length == 18) {
            var idCardWi = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2); //将前17位加权因子保存在数组里
            var idCardY = new Array(1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2); //这是除以11后，可能产生的11位余数、验证码，也保存成数组
            var idCardWiSum = 0; //用来保存前17位各自乖以加权因子后的总和
            for (var i = 0; i < 17; i++) {
                idCardWiSum += idCard.substring(i, i + 1) * idCardWi[i];
            }
            var idCardMod = idCardWiSum % 11;//计算出校验码所在数组的位置
            var idCardLast = idCard.substring(17);//得到最后一位身份证号码
            //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
            if (idCardMod == 2) {
                if (idCardLast == "X" || idCardLast == "x") {
                    return true
                } else {
                    return false
                }
            } else {
                //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
                if (idCardLast == idCardY[idCardMod]) {
                    return true
                } else {
                    return false
                }
            }
        }
    } else {
        return false
    }
}
/**
 * 7校验护照（港澳通行证）是否正确
 * @param {*} passport 
 */
export function validatePassport(passport) {
    var reg = /(^[EeKkGgDdSsPpHh]\d{8}$)|(^(([Ee][a-fA-F])|([DdSsPp][Ee])|([Kk][Jj])|([Mm][Aa])|(1[45]))\d{7}$)/
    if (reg.test(passport)) {
        return true
    } else {
        return false
    }
}

/**
 * 8银行卡号四位分割
 * @param bankCard
 * @returns {string}
 */
export function formatBankCard(bankCard) {
    if (bankCard && bankCard.length > 3) {
        let length = Math.floor(bankCard.length / 4);
        let resBankCard = "";
        for (let i = 0; i < length; i++) {
            resBankCard += bankCard.slice(i * 4, (i + 1) * 4) + " "
        }
        return resBankCard
    } else {
        return bankCard
    }
}